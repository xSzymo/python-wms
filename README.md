## Time tracer
You can manage your required time / include time within specified past days by 2 parameters - time and days from url. <br>
Example : for required 10 hours & include time from last 7 days :<br>
localhost:5000/?time=600&days=7<br>
Also you can edit values from db by edit site :<br>
localhost:5000/edit

### How to run with docker(demo) :
- make sure your port 5000 is free
- copy docker-compose.yml file from this repository to your local environment
- locate docker-compose.yml file and run command in same directory : docker-compose up
- get onto localhost:5000/?time=600&days=7
