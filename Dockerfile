FROM python:3.6

WORKDIR /app

COPY . /app

RUN pip install Flask
RUN pip install flask-cors --upgrade
RUN pip install flask-restful
RUN pip install SQLAlchemy
RUN pip install mysqlclient

ENV NAME World

CMD ["python", "timer/main.py"]
