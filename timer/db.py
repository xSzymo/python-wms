from sqlalchemy import Column, Integer, String
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

#engine = create_engine('mysql://root:admin@localhost:3306/timer')
engine = create_engine('mysql://root:admin@db:3306/test')

engine.connect()
Session = sessionmaker(bind=engine)
Base = declarative_base()
session = Session()

def add(data, time):
    foundTimers = getByDate(data)
    lastTime = 0
    if len(foundTimers) > 0:
        for x in foundTimers:
            lastTime += int(x.time)
        for x in foundTimers:
            deleteByDate(x.data)

    lastTime += int(time)
    session.add(Timer(data=data, time=lastTime))
    session.commit()


def getAll():
    session.expire_all()
    return session.query(Timer).all()


def getByDate(data):
    session.expire_all()
    return session.query(Timer).filter(Timer.data == data).all()


def deleteByDate(data):
    timers = getByDate(data)
    for x in timers:
        session.delete(x)
        session.commit()


def deleteAll():
    timers = getAll()
    for x in timers:
        session.delete(x)
        session.commit()


def setup():
    try:
        session.execute("CREATE TABLE timer (id int PRIMARY KEY AUTO_INCREMENT,data date,time int);")
    except:
        print("Tables already exists")

class Timer(Base):
    __tablename__ = 'timer'
    id = Column(Integer, primary_key=True)
    data = Column('data', String(32))
    time = Column('time', Integer)
