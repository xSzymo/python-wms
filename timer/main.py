from __future__ import print_function

import datetime
import time
from time import sleep

from flask import Flask
from flask import jsonify
from flask import render_template
from flask import request

from db import getAll, deleteByDate, add, deleteAll, setup

app = Flask(__name__)
status = False
times = []

defaultDays = 7

'''
# SETUP :
# Configure db connection in db.py also check table names
#
# RUN :
# run by : FLASK_APP=main.py FLASK_DEBUG=0 python -m flask run
#
# FUNCTIONALITY :
#    - home page
#    - edit page
#    - status - currently in/out (logged)
#    - log in / log out
#    - check elapsed period of time
#    - add record
#    - delete (all) record/s
#
# TODO :
#   - status in db
#
#
# NOTE :
# Possibly to modify returned days or required time by parameters : ?days=14&time=1200
#
'''


@app.route("/")
def hello():
    print("Mapping : '/' [ From : " + str(request.remote_addr) + " | Days to return : ", end='')
    days = defaultDays
    try:
        print(int(request.args.get('days')), end='')
        days = int(request.args.get('days'))
    except:
        print("" + str(defaultDays), end='')
    print(" ]\n")

    entities = getAll()
    tab = []
    for x in entities:
        halo = [x.data, x.time]
        tab.append(halo)

    tab.sort(key=lambda r: r[0], reverse=True)
    return render_template('index.html', tab=tab[:days])


@app.route("/status")
def current_status():
    global status
    print("Mapping : '/status' [ From : " + request.remote_addr + " | Status : " + str(status) + " ]\n")
    return jsonify({'status': status})


@app.route("/in")
def login():
    global status
    print("Mapping : '/in' [ From : " + request.remote_addr + " ]\n")
    status = True
    times[:] = []
    times.append(int(time.time() * 1000.0))

    return ""


@app.route("/out")
def logout():
    global status
    print("Mapping : '/out' [ From : " + request.remote_addr + " | ", end='')

    status = False

    current_time = (int(time.time() * 1000.0) - times[0])
    if current_time > 0:
        time_elapsed = current_time / 1000
        add(str(datetime.datetime.utcnow().year) + "-" + str(datetime.datetime.utcnow().month) + "-" + str(
            datetime.datetime.utcnow().day), time_elapsed)
        times[:] = []
        print("Elapsed time : " + str(time_elapsed) + "sec ]\n")

    return ""


@app.route("/period")
def period():
    print("Mapping : '/period' [ From : " + request.remote_addr + " | ", end='')

    time_elapsed = 0
    try:
        current_time = (int(time.time() * 1000.0) - times[0])
        if current_time > 0:
            time_elapsed = current_time / 1000
            print("Elapsed time : " + str(time_elapsed) + "sec ]\n")
    except IndexError:
        time_elapsed = 0

    return jsonify({'time': time_elapsed})


@app.route("/add", methods=["POST"])
def add_record():
    print("Mapping : '/add' [ From : " + request.remote_addr + " | Date " + request.form['date'] + " | Time : " +
          request.form['time'] + "min ] \n")
    add(request.form['date'], (int(request.form['time']) * 60))
    return ""


@app.route("/edit")
def edit_page():
    print("Mapping : '/edit' [ From : " + request.remote_addr + " ]\n")
    return render_template('edit.html')


@app.route("/delete", methods=["POST"])
def delete_rbxcecord():
    print("Mapping : '/delete' [ From : " + request.form['date'] + " ]\n")
    deleteByDate(request.form['date'])
    return ""


@app.route("/deleteAll", methods=["GET"])
def delete_record():
    print("Mapping : '/deleteAll' [ From : " + request.remote_addr + " ]\n")
    deleteAll()
    return ""

if __name__ == '__main__':
    sleep(5)
    setup()
    app.run(host='0.0.0.0')
